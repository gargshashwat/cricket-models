
import argparse
import logging
from cricket_models.scrapers import batting_performances, play_by_play, t20_match_scraper


def configure_logging(is_verbose):

    log_level = logging.INFO
    if is_verbose:
        log_level = logging.DEBUG
    logging.basicConfig(
        level=log_level,
        format="%(asctime)s [%(levelname)-5.5s]  %(message)s",
        handlers=[
            logging.StreamHandler(),
        ]
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'scraper',
        choices=['batting_performances', 'play_by_play', 't20_match_scraper'],
        help='Which scraper to run.'
    )
    parser.add_argument(
        '--year',
        type=int,
        help='Year for t20_match_scraper.',
    )
    parser.add_argument(
        '--input-file-name',
        help='Input file for play_by_play scraper.',
    )
    parser.add_argument(
        '--restrict-host-country-ids',
        help='List of country ids to fetch for the play_by_play scraper.',
        nargs='*'
    )
    parser.add_argument(
        '-v', '--verbose',
        help='Debug level logging.',
        action='store_true'
    )
    args = parser.parse_args()
    configure_logging(args.verbose)

    # Pick out the parser to run.
    if args.scraper == 'batting_performances':
        batting_performances.main()
    elif args.scraper == 't20_match_scraper':
        t20_match_scraper.main(args.year)
    elif args.scraper == 'play_by_play':
        play_by_play.main(args.input_file_name, args.restrict_host_country_ids)
