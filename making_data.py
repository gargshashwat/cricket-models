#!/usr/bin/env python
# coding: utf-8

# ### Imports

import pandas as pd
import numpy as np
import json
from os import listdir
from os.path import isfile, join, isdir

def get_list_of_files(dirName):
    """ create a list of all files with their paths 
    in the given directory.
    """
    list_of_file = listdir(dirName)
    all_files = list()
    # Iterate over all the entries
    for entry in list_of_file:
        # Create full path
        full_path = join(dirName, entry)
        # If entry is a directory then get the list of files in this directory 
        if isdir(full_path):
            all_files = all_files + get_list_of_files(full_path)
        else:
            try:
                with open(full_path) as data_file:    
                    data = json.load(data_file)
                all_files.append(full_path)
            except:
                continue     
                
    return all_files  


def id_from_path(path):
    b_id = ''
    components = path.split('/')
    for c in components[3: -1]:
        b_id += c
    return b_id


""" Get a list of all matches """
mypath = 'var/data/matches/'
list_of_files = get_list_of_files(mypath)

""" Getting the relevant features for every ball and the batsman id for the batsman on strike """

TOTAL_BALLS_IN_INNINGS = 120
TOTAL_WICKETS_IN_INNINGS = 10

columns = ['date', 'ball', 'total_wickets', 'batsman_athlete_id', 'batsman_runs', 'wickets', 'runs', 'total_runs']
df_innings_one = pd.DataFrame(columns=columns)
df_innings_two = pd.DataFrame(columns=columns)


for file in list_of_files:
    with open(file) as data_file:    
        data = json.load(data_file)
    first_innings_score = data['info']['first_innings_score']
    data = data['innings']    
    df = pd.DataFrame(data[0]['balls'])[columns]
    df['first_innings_score'] = first_innings_score
    df_innings_one = pd.concat([df_innings_one, df], sort=True)
    df = pd.DataFrame(data[1]['balls'])[columns]  
    df['first_innings_score'] = first_innings_score
    df_innings_two = pd.concat([df_innings_two, df], sort=True)

columns.append('innings')
columns.append('first_innings_score')    
df_innings_one['innings'] = 1
df_innings_two['innings'] = 2

df_innings_one.reset_index(inplace=True)
df_innings_one = df_innings_one[columns]
df_innings_two.reset_index(inplace=True)
df_innings_two = df_innings_two[columns]

df_balls = pd.DataFrame(columns=columns)
df_balls = pd.concat([df_innings_one, df_innings_two], sort=True)
df_balls.reset_index(inplace=True)
df_balls = df_balls[columns]
df_balls['balls_remaining'] = TOTAL_BALLS_IN_INNINGS - df_balls['ball']
df_balls['total_wickets_remaining'] = TOTAL_WICKETS_IN_INNINGS - df_balls['total_wickets']



""" Clean up data a bit and save"""
df_balls = df_balls.loc[df_balls['ball'] > 0 ]
df_balls = df_balls.loc[df_balls['ball'] <= 120 ]

df_balls.to_csv('ball_data.csv')
print("Got info for balls.")


""" Get a list of all players """
mypath = 'var/data/players/'
list_of_files_players = get_list_of_files(mypath)


""" Save player stats """
columns = ['playerId', 'date', 'ballsFaced', 'runs', 'outs']
df_players = pd.DataFrame(columns=columns)

index = 0
for file in list_of_files_players:
    with open(file) as data_file:    
        data = json.load(data_file)
    b_id = id_from_path(file)
    data = data['innings']
    for inning in data.values():
        df_players.loc[index] = [b_id, inning['date'], inning['ballsFaced'], inning['runs'], inning['outs']]
        index += 1

df_players.to_csv('player_data.csv')
print("Got info for players.")


""" Add current batsman stats to every ball """ 

def get_batsman_stats_one(x):
    temp = df_players.loc[df_players['playerId'] == x.batsman_athlete_id ]
    temp = temp.loc[temp['date'] < x.date]
    return np.sum(temp['ballsFaced'])

def get_batsman_stats_two(x):
    temp = df_players.loc[df_players['playerId'] == x.batsman_athlete_id ]
    temp = temp.loc[temp['date'] < x.date]
    return np.sum(temp['runs'])

def get_batsman_stats_three(x):
    temp = df_players.loc[df_players['playerId'] == x.batsman_athlete_id ]
    temp = temp.loc[temp['date'] < x.date]
    return np.sum(temp['outs'])
    
df_balls['batsman_career_balls_faced'] =  df_balls.apply(get_batsman_stats_one, axis = 1)
df_balls['batsman_career_runs'] =  df_balls.apply(get_batsman_stats_two, axis = 1)
df_balls['batsman_career_outs'] =  df_balls.apply(get_batsman_stats_three, axis = 1)

df_balls.to_csv('ball_player_data.csv')
print("Combined them. Finished")

