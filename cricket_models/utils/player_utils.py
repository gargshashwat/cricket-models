"""
Utility functions to help specifically with player json manipulations
"""

from cricket_models.utils.request_utils import get_page, get_soup
from cricket_models.datatypes.player import Player


def add_player_batting_stats(player,
                             event_id,
                             date,
                             innings,
                             logger,
                             save_as_dict):
    # print(player)
    info = player['athlete']
    id = info['id']
    name = info['name']
    # print(id, name)
    logger.info('Collecting batting info for %s:%s', id, name)
    ### add player's batting score to his history
    stats = player['linescores'][innings]['statistics']['categories'][0]['stats']
    stat_lookup = dict([(s['name'], s.get('value')) for s in stats])
    if save_as_dict:
        # stat_set = stat_lookup
        stat_set = {}
        stat_set['event_id'] = event_id
        stat_set['date'] = date

        stat_set['battingPosition'] = stat_lookup['battingPosition']
        stat_set['runs'] = stat_lookup['runs']
        stat_set['ballsFaced'] = stat_lookup['ballsFaced']
        stat_set['fours'] = stat_lookup['fours']
        stat_set['sixes'] = stat_lookup['sixes']
        stat_set['dots'] = stat_lookup['dots']
        stat_set['batted'] = stat_lookup['batted']
        stat_set['highScore'] = stat_lookup['highScore']
        stat_set['innings'] = stat_lookup['innings']
        stat_set['dismissal'] = stat_lookup['dismissal']
        stat_set['minutes'] = stat_lookup['minutes']
        stat_set['notouts'] = stat_lookup['notouts']
        stat_set['outs'] = stat_lookup['outs']
        stat_set['strikeRate'] = stat_lookup['strikeRate']

        stat_set['battingId'] = stat_lookup['battingId']
        stat_set['inningsId'] = stat_lookup['inningsId']
        stat_set['matchPlayerId'] = stat_lookup['matchPlayerId']
        stat_set['teamId'] = stat_lookup['teamId']
        stat_set['oppositionId'] = stat_lookup['oppositionId']
    else:
        stat_set = [
            date,
            '-' not in stat_lookup['highScore'] and 1 or 0,
            stat_lookup['runs'],
            stat_lookup['ballsFaced'],
            stat_lookup['notouts']
        ]
    # TODO - this would be better cached, rather than read/loaded constantly;
    #  likely a LRU would be nice.
    player = Player(
        id,
        player_info={'name': name}
    )
    player.add_innings(event_id, stat_set)
    return player


def get_batting_performance(json_link,
                          class_id,
                          event_id,
                          logger,
                          save_as_dict):
    match_json_link = json_link.format(
        class_id=class_id,
        event_id=event_id
    )
    logger.info(match_json_link)
    match_data = get_page(match_json_link, cacheable=True, as_json=True)

    if not match_played(match_data):
        return
    match_name = ' vs '.join([r['team']['displayName'] for r in match_data['rosters']])
    date = get_date_from_json(match_data)
    home_team = match_data['rosters'][0]['roster']

    ### check which innings home_team were batting in
    innings = 0
    temp = home_team[0]['linescores'][0]['statistics']['categories'][0]['stats'][0]['name']
    if temp == 'balls':
        innings = 1
    logger.info('Getting stats for %s on %s', match_name, date)

    ### player statistics
    home_team_batting_order = []
    for player in home_team:
        player = add_player_batting_stats(player,
                                          event_id,
                                          date,
                                          innings,
                                          logger,
                                          save_as_dict)
        home_team_batting_order.append(player.player_id)

    away_team = match_data['rosters'][1]['roster']
    away_team_batting_order = []
    # Subtract one from innings to get the batting info.
    for player in away_team:
        player = add_player_batting_stats(player,
                                          event_id,
                                          date, 1 - innings,
                                          logger,
                                          save_as_dict)
        away_team_batting_order.append(player.player_id)

    return (home_team_batting_order, away_team_batting_order) if innings == 0 else (away_team_batting_order, home_team_batting_order)


def get_date_from_json(match_data):
    return match_data['header']['competitions'][0]['date']


def match_played(match_data):
    return len(match_data['rosters'][0]['roster'][0]['linescores']) != 0
