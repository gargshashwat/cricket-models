import requests
import logging
import os
import json
import time
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from slugify import slugify


from cricket_models.settings import IGNORE_CACHE, CACHE_DIR
logger = logging.getLogger(__name__)


def _url_to_cache_location(url):
    """
    Make the URL into a file-system friendly version.
    """
    parsed = urlparse(url)
    output_dir = os.path.join(*[
        CACHE_DIR,
        parsed.scheme,
        parsed.netloc,
        slugify(parsed.path),
        slugify(parsed.query)
    ])
    return output_dir


def _check_cache(url):
    """
    Check the cache for this request.
    """
    cache_path = _url_to_cache_location(url)
    cache_file = os.path.join(cache_path, 'data.cache')
    if os.path.exists(cache_file):
        with open(cache_file, 'rb') as f:
            return f.read()
    return None


def _add_cache(url, content):
    """
    Save a cache entry.
    """
    cache_path = _url_to_cache_location(url)
    os.makedirs(cache_path, exist_ok=True)
    cache_file = os.path.join(cache_path, 'data.cache')

    with open(cache_file, 'wb') as f:
        f.write(content)
    # Also write out the url for debugging.
    info_file = os.path.join(cache_path, 'info.txt')
    with open(info_file, 'w') as f:
        f.write(url)
    return None


def get_page(url, cacheable=False, as_json=False, num_retries=0, max_retries=3, delay=2):
    """
    Returns a response from getting a page.
    Centralised here in case we need proxies/sessions/retries/etc.

    # TODO
    - retries
    - proxies
    """
    if cacheable and not IGNORE_CACHE:
        content = _check_cache(url)
        if content:
            logger.debug('Retrieved from cache: %s', url)
            if as_json:
                return json.loads(content)
            return content

    # Go and get it normally.
    logger.debug('GETing %s', url)
    response = requests.get(url)
    if not response.ok:
        # Attempt to retry a few times (3 default)
        if num_retries >= max_retries:
            # logger.error('Got bad response: %s for url: %s', response, url)
            # return None
            raise ValueError('Got bad response: %s for url: %s', response, url)
        # Re-Call ourselves after a short delay.
        time.sleep(delay)
        return get_page(
            url,
            cacheable=cacheable,
            as_json=as_json,
            num_retries=num_retries + 1,
            max_retries=max_retries,
            delay=delay
        )

    content = response.content
    # And add it to our cache.
    if cacheable:
        _add_cache(url, content)

    if as_json:
        return json.loads(content)
    return content


def get_soup(url, cacheable=False):
    """
    Central method for getting the soup rep of a page.

    We may want to switch to lxml parser for fastness.
    """
    return BeautifulSoup(get_page(url, cacheable=cacheable), 'html.parser')
