"""
Utility functions to help with the scraping
"""
import re
import logging

from cricket_models.utils.request_utils import get_soup

logger = logging.getLogger(__name__)


def get_all_id_values(base_link,
                      input_match_type_class,
                      restrict_host_ids=None):
    """Given a input_match_type_class return all id_values for us to scrape
    input_match_type_class refers to the type of match (Test matches, ODIs, T20s etc)
    """
    class_link = base_link.format(match_type_class=input_match_type_class)
    soup = get_soup(class_link, cacheable=True)
    aas = soup.find_all("a", {"class", "RecordLinks"})

    id_values = []
    for a in aas:
        href_link = a['href']
        if "type=host" in href_link:
            parameters_search = re.search(
                '/ci/engine/records/index.html\?class=(.+?);id=(.+?);type=host',
                href_link
            )
            if parameters_search:
                # if counter < 1:
                class_value = parameters_search.group(1)
                id_value = parameters_search.group(2)
                if not restrict_host_ids or \
                    id_value in restrict_host_ids:
                    logger.info('Including Class: %s id_value %s (%s)', class_value,
                        id_value, a.text)
                    id_values.append(id_value)
                else:
                    logger.info('Excluding: class: %s id_value: %s (%s)',
                        class_value, id_value, a.text)
    return id_values


def get_season_links(stats_prefix,
                     match_results_base_link,
                     match_type_class,
                     id_values):
    """ Given the match_type_class and id_value, get season link.
    A season refers to the year T20s were played in the particular country/tournament.
    This is achieved by replacing the class and id values in the link:
    http://stats.espncricinfo.com/ci/engine/records/team/match_results_season.html?class=X;id=Y;type=host
    """
    season_links = []
    for id_value in id_values:
        logger.info('Loading match results links for %s / %s', match_type_class, id_value)
        match_results_link = match_results_base_link.format(
            klass=match_type_class,
            id=id_value
        )
        soup = get_soup(
            match_results_link,
            cacheable=True
        )
        temp = soup.find_all("a")
        for a in temp:
            href_link = a['href']
            if "match_results.html" in href_link:
                url = '{}{}'.format(stats_prefix, href_link)
                season_links.append(url)
    return season_links


def get_api_link_from_scorecard_link(scorecard_link):
    """Given the scorecard link, get the API link which returns a json from which we can get the required info
    """
    event_id_search = re.search('match/(.+?).html', scorecard_link)
    if (event_id_search):
        event_id = event_id_search.group(1)
    return 'http://site.api.espn.com/apis/site/v2/sports/cricket/1/playbyplay?contentorigin=espn&event=' + event_id + '&lang=en&page={page}&period={period}&region=gb&section=cricinfo'


def get_ground_name(soup):
    """Get ground name for a match from the input soup handl
    """
    ground_element = soup.find_all("div", {"class": "stadium-details"})
    ground = ground_element[0].find_all("a")[0].text
    return ground


def get_team_name(soup2, href):
    """Get team name for a match from the input soup handle
    """
    a = soup2.find_all("a", {"href": href})
    return a[0].text.replace(" Innings", "").strip()


def get_batting_team_names(soup2):
    """Returns the names of the teams that played in the match
    """
    batting_first_team_name = get_team_name(soup2, "#gp-inning-00")
    chasing_team_name = get_team_name(soup2, "#gp-inning-01")
    return [batting_first_team_name, chasing_team_name]


def get_innings_scores(soup, innings):
    """Given a soup handle and innings, get the innings score
    """
    innings_scores = soup.select("div.cscore_score")
    if len(innings_scores) >= 0:
        if innings == 1:
            innings_score = innings_scores[0].text.rsplit('/')[0].rsplit(' ')[0]
        else:
            innings_score = innings_scores[1].text.rsplit('/')[0].rsplit(' ')[0]
        return int(innings_score)
    return -1


def get_tournament_name(soup, team_names):
    """Given the team names, figure out the tournament. This will help
    with looking for patterns at a tournament level
    """
    [batting_first_team_name, chasing_team_name] = team_names
    if batting_first_team_name in xi_teams or chasing_team_name in xi_teams:
        tournament = "xi_teams"
    elif batting_first_team_name in a_teams or chasing_team_name in a_teams:
        tournament = "a_teams"
    elif batting_first_team_name in international_teams or chasing_team_name in international_teams:
        tournament = "Internationals"
    else:
        match_detail_items = soup.find_all("div", {"class": "match-detail--item"})
        tournament = match_detail_items[0].find_all("div", {"class": "match-detail--right"})[0].text
    return tournament


def process_athlete_json(input_athlete_json):
    """Retrieve athelete_id from input json
    """
    athlete_id = input_athlete_json["id"] if len(input_athlete_json) > 0 else -1
    return athlete_id


def process_batsman_json(input_json):
    """Given an input_json for a particular ball, figure out what score a batsman
    is on and how many deliveries he has faced in the innings.
    """
    athlete_json = input_json["athlete"]
    athlete_id = process_athlete_json(athlete_json)
    batsman_runs = input_json["totalRuns"]
    batsman_balls_faced = input_json["faced"]
    return (athlete_id, batsman_runs, batsman_balls_faced)


def process_bowler_json(input_json):
    """Given an input_json for a particular ball, figure out the bowler figures
    for the innings
    """
    athlete_json = input_json["athlete"]
    athlete_id = process_athlete_json(athlete_json)
    bowler_runs = input_json["conceded"]
    bowler_wickets = input_json["wickets"]
    bowler_balls = input_json["balls"]
    return (athlete_id, bowler_runs, bowler_wickets, bowler_balls)


def is_full_completed_match(soup):
    """ We only want to capture data for fully completed T20 matches.
    This function returns True only if the match has been fully completed, non-rain interrupted
    etc. Else it returns false
    """
    no_match_result = "Did not find Match Result"
    match_result = no_match_result
    for i in range(20):
        try:
            found = soup.find_all("div", {"class": "cscore_notes"})
            match_result = found[0].text
            break
        except:
            is_fourofour = soup.find_all("section", {"class": "col-a Error404"})
            if len(is_fourofour) > 0:
                temp = "404 Error"
            else:
                body = soup.find_all("body")
                if len(body) > 0 and "504" in body[0].text:
                    temp = "504 Error"
                else:
                    temp = "Uncaptured error"
                    logging.error(str(soup))
                    logging.error(str(found))
            logging.error(temp)

    match_abandoned = "Match abandoned"
    no_result = "No result"
    dl_method = "D/L method"
    cancelled = "cancelled"
    question = "?"
    if((no_match_result in match_result) or (match_abandoned in match_result) or (no_result in match_result) or (dl_method in match_result) or (cancelled in match_result) or (question in match_result)):
        logging.warning('Match result: %s', match_result)
        # print(match_result)
        return match_result, False

    first_innings_team_name = soup.find_all("a", {"href": "#gp-inning-00"})
    second_innings_team_name = soup.find_all("a", {"href": "#gp-inning-01"})
    if(len(first_innings_team_name) == 0 or len(second_innings_team_name) == 0):
        return match_result, False
    return match_result, True


def get_result(first_innings_score, second_innings_score, match_result):
    """
    Given the first innings and second innings scores, figure out who won the match
    """
    if (("tied" in match_result) and first_innings_score != second_innings_score) or (("tied" not in match_result) and first_innings_score == second_innings_score):
        print("Something is wrong. first_innings_score %d, second_innings_score %d, match_result %s" %
              (first_innings_score, second_innings_score, match_result))
    if(first_innings_score > second_innings_score):
        return 0
    elif(first_innings_score < second_innings_score):
        return 1
    elif(first_innings_score == second_innings_score):
        return 2
    else:
        return -1


def convert_to_appropriate_input(name):
    return name.replace(" ", "")


def get_class_event_ids(scorecard_link, logger):
    soup = get_soup(scorecard_link, cacheable=True)
    tds = soup.find_all("link", {"rel": "canonical"})
    if len(tds)>1:
        raise Exception("More than one link with rel = canonical")
    for td in tds:
        href_link = td['href']
        class_id=href_link.split('/')[4]
        event_id=href_link.split('/')[6]
        return (class_id, event_id)


international_teams = ['Afghanistan', 'Australia', 'Bangladesh', 'Bermuda', 'Canada', 'England', 'HongKong', 'India', 'Ireland', 'Kenya', 'Nepal',
                       'Netherlands', 'New Zealand', 'Oman', 'Pakistan', 'PapuaNewGuinea', 'Scotland', 'South Africa', 'Sri Lanka', 'United Arab Emirates', 'West Indies', 'World-XI', 'Zimbabwe']

a_teams = ['Australia A', 'Pakistan A', 'SriLanka A', 'Bangladesh A',
           'India A', 'WestIndies A', 'NewZealand A', 'SouthAfrica A']
xi_teams = ['CricketAustraliaXI', 'PrimeMinister\'sXI', 'BangladeshCricketBoardXI', 'PCAMastersXI', 'NewZealandXI',
            'ZimbabwePresident\'sXI', 'SriLankaCricketCombinedXI', 'WestIndiesXI', 'WestIndiesCricketBoardPresident\'sXI']
