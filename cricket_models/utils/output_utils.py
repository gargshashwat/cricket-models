"""
Utility functions to help scraper output to files
"""

from cricket_models.utils import utils


def write_log_file(log_file, log_data):
    for log_row in log_data:
        print(log_row)
        log_file.write(log_row)
        log_file.write('\n')


def write_data(dataset, lr_data_file, innings):
    global num_of_writes
    for counter, inningData in enumerate(dataset):
        for this_innings_counter, data in enumerate(inningData):
            to_write = ""
            if num_of_writes == 0 and counter == 0 and this_innings_counter == 0:
                to_write += "GameId\t"
                if innings == 2:
                    to_write += "FirstInningsScore\t"
                to_write += "Ball\tTotalRuns\tTotalWickets\tRuns\tWickets\tBatsmanId\tBatsmanRuns\t\
                                BatsmanBallsFaced\tOtherBatsmanId\tOtherBatsmanRuns\tOtherBatsmanBallsFaced\t\
                                BowlerId\tBowlerRuns\tBowlerWickets\tBowlerBalls\tFirstInningsTeam\t\
                                SecondInningsTeam\tTournament\tGround\tTime\tResult\n"
                lr_data_file.write(to_write)
                to_write = ""
                to_write = ""

            if data.innings == innings:
                to_write += str(data.game_id) + '\t'
                if data.innings == 2:
                    to_write += str(data.first_innings_score) + '\t'
                to_write += str(data.ball) + '\t'
                to_write += str(data.total_runs) + '\t'
                to_write += str(data.total_wickets) + '\t'
                to_write += str(data.runs) + '\t'
                to_write += str(data.wickets) + '\t'
                to_write += str(data.batsman_athlete_id) + '\t'
                to_write += str(data.batsman_runs) + '\t'
                to_write += str(data.batsman_balls_faced) + '\t'
                to_write += str(data.other_batsman_athlete_id) + '\t'
                to_write += str(data.other_batsman_runs) + '\t'
                to_write += str(data.other_batsman_balls_faced) + '\t'
                to_write += str(data.bowler_athlete_id) + '\t'
                to_write += str(data.bowler_runs) + '\t'
                to_write += str(data.bowler_wickets) + '\t'
                to_write += str(data.bowler_balls) + '\t'

                to_write += utils.convert_to_appropriate_input(data.first_innings_team) + '\t'
                to_write += utils.convert_to_appropriate_input(data.second_innings_team) + '\t'
                to_write += utils.convert_to_appropriate_input(data.tournament_name) + '\t'
                to_write += utils.convert_to_appropriate_input(data.ground_name) + '\t'

                to_write += data.date + '\t'
                to_write += str(data.result)
                to_write += '\n'

                if (innings == 2 and data.ball < 120) or (innings == 1):
                    lr_data_file.write(to_write)


def write_all_data(override_flag, input_file_name, all_matches, log_data, firstinnings_data_file_name, chasing_data_file_name):
    global num_of_writes
    if len(all_matches) % 50 == 0 or override_flag:
        # print("Writing..")
        permission = "a"
        if num_of_writes == 0 and input_file_name is None:
            permission = "w+"

        firstinnings_data_file = open(firstinnings_data_file_name, permission)
        chasing_training_data_file = open(chasing_data_file_name, permission)

        log_file = open("ScraperLogFile", permission)
        write_log_file(log_file, log_data)
        log_file.close
        log_data = []

        write_data(all_matches, firstinnings_data_file, 1)
        firstinnings_data_file.close
        write_data(all_matches, chasing_training_data_file, 2)
        chasing_training_data_file.close

        num_of_writes = num_of_writes + 1
        all_matches = []
    return all_matches, log_data


num_of_writes = 0
