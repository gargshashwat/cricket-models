import re
import logging

from cricket_models.utils import utils
from cricket_models.utils import player_utils
from cricket_models.datatypes.player import Player
from cricket_models.utils.request_utils import get_page, get_soup
from cricket_models.utils.player_utils import get_batting_performance

logger = logging.getLogger(__name__)


# Run through each T20 match.

# We will make a json dictionary with one entry for each player. For each player, we will store entries for each match played:


# {
# p1_id: {name:, history: [(date of match, played, runs scored, balls faced, NO), ..., ()]}
# p2_id:
# .
# .
# .
# }


BASE_LINK = 'http://stats.espncricinfo.com/ci/engine/records/index.html?class={match_type_class}'
MATCH_RESULTS_BASE_LINK = 'http://stats.espncricinfo.com/ci/engine/records/team/match_results_season.html?class={klass};id={id};type=host'
STATS_PREFIX = 'http://stats.espncricinfo.com'
JSON_LINK = 'http://site.web.api.espn.com/apis/site/v2/sports/cricket/{class_id}/summary?contentorigin=espn&event={event_id}&lang=en&region=uk&section=cricinfo'

# WHether to save the players batting stats as a dict, or an unnamed list.
SAVE_AS_DICT = True

# Refers to the match type the scraper should be run for.
# 1 = Test matches, 2 = ODIs, 3 = International T20s, 6 = All T20s (inlcuding Internationals)
MATCH_TYPE_CLASS = 6

def scrape_matches_from_season_link(season_link):
    """From the match results page, get every single match result one-by-one
    and call capture_scorecard_data
    """
    # This one is only kind of cacheable, so hence not.  If it's a current year
    # it will likely get new matches added.
    soup = get_soup(
        season_link,
        cacheable=False,
    )
    tds = soup.find_all("td", {"nowrap": "nowrap"})
    for td in tds:
        aas = td.find_all("a", {"class": "data-link"})
        for a in aas:
            tag_text = a.text
            if "T20" in tag_text:
                href_link = a['href']
                try:
                    # print(STATS_PREFIX + href_link)
                    (class_id, event_id) = utils.get_class_event_ids(
                        STATS_PREFIX + href_link,
                        logger)
                except Exception:
                    logger.error("Problem with getting class and event ID " + STATS_PREFIX + href_link)
                    return
                logger.info("Match class_id = " + class_id + ", " + "match event_id = " + event_id)
                try:
                    player_utils.get_batting_performance(JSON_LINK,
                                                         class_id,
                                                         event_id,
                                                         logger,
                                                         SAVE_AS_DICT)
                except Exception as e:
                    logger.exception("Problem with class_id: %s, event_id: %s ",
                                     class_id,
                                     event_id)
                    continue


def start_scrape(restrict_host_ids=None):
    logger.info('Starting scrape')
    id_values = utils.get_all_id_values(BASE_LINK,
                                        MATCH_TYPE_CLASS,
                                        restrict_host_ids)
    logger.info('Retrieved all id values')
    season_links = utils.get_season_links(STATS_PREFIX,
                                          MATCH_RESULTS_BASE_LINK,
                                          MATCH_TYPE_CLASS,
                                          id_values)
    logger.info('Retrieved season links')
    for counter, season_link in enumerate(season_links):
        scrape_matches_from_season_link(season_link)


def main(restrict_host_ids=None):
    start_scrape(restrict_host_ids)


if __name__ == '__main__':
    main(restrict_host_ids=("7"))
