"""
This module scrapes all T20 matches from the website: www.cricinfo.com and provides the relevant ball specific data
so that we can replicate the paper: https://pdfs.semanticscholar.org/8f13/8b816d58f8c2d2d4b3527906c125059d9e3a.pdf
"""

import re
import logging
import argparse

from cricket_models.datatypes.ball_data import BallData
from cricket_models.datatypes.match import Match
from cricket_models.datatypes.match import Innings
from cricket_models.utils import utils
from cricket_models.utils import output_utils
from cricket_models.utils import player_utils
from cricket_models.utils.request_utils import get_page, get_soup

logger = logging.getLogger(__name__)
logging.basicConfig(filename='example.log', level=logging.WARNING)

BASE_LINK = 'http://stats.espncricinfo.com/ci/engine/records/index.html?class={match_type_class}'
MATCH_RESULTS_BASE_LINK = 'http://stats.espncricinfo.com/ci/engine/records/team/match_results_season.html?class={klass};id={id};type=host'
STATS_PREFIX = 'http://stats.espncricinfo.com'
JSON_LINK = 'http://site.web.api.espn.com/apis/site/v2/sports/cricket/{class_id}/summary?contentorigin=espn&event={event_id}&lang=en&region=uk&section=cricinfo'

# WHether to save the players batting stats as a dict, or an unnamed list.
SAVE_PLAYERS_AS_DICT = True

# Refers to the match type the scraper should be run for.
# 1 = Test matches, 2 = ODIs, 3 = International T20s, 6 = All T20s (inlcuding Internationals)
MATCH_TYPE_CLASS = 6

def is_accurate_scorecard(match):
    """ This function attempts to check that the ball-by-ball data matches the scorecard totals.
    For some scorecards, the ball-by-ball data is not complete. Certain deliveries can be missing
    and the score might be inaccurate. This function returns True only if the scorecard is accurate.
    """
    for innings_data in match.innings:

        innings_data.balls.sort(key=lambda x: (x.ball, x.total_runs, x.total_wickets))

        prev_ball = 0
        prev_total_runs = 0
        prev_total_wickets = 0
        for ball_data in innings_data.balls:
            ball = ball_data.ball
            total_runs = ball_data.total_runs
            total_wickets = ball_data.total_wickets
            if (ball - prev_ball) > 1:
                temp = "More than one ball missing, innings %d, ball %d, prev_ball %d" % (
                    innings_data.innings_number, ball, prev_ball)
                # log_data.append(temp)
            if total_runs < prev_total_runs:
                temp = "total_runs < prev_total_runs, innings %d, balls %d, total runs %d, previous total runs %d" % (
                    innings_data.innings_number, ball, total_runs, prev_total_runs)
                # log_data.append(temp)
            if total_wickets < prev_total_wickets:
                temp = "total_wickets < prev_total_wickets, innings %d, total_wickets %d, prev_total_wickets %d" % (
                    innings_data.innings_number, prev_total_wickets, total_wickets)
                # log_data.append(temp)
            prev_ball = ball
            prev_total_runs = total_runs
            prev_total_wickets = total_wickets

        api_total_runs = innings_data.balls[len(innings_data.balls) - 1].total_runs
        summarised_innings_score = match.match_info['first_innings_score']
        if innings_data.innings_number == 2:
            summarised_innings_score = match.match_info['second_innings_score']

        if api_total_runs != summarised_innings_score:
            log_text = "innings %d, api_total_runs %d != summarised_innings_score %d" % \
                (innings_data.innings_number, api_total_runs, summarised_innings_score)
            # log_data.append(log_text)
            logger.error(log_text)
            return False
    return True


def add_ball_data(innings, item):
    innings_json = item["innings"]
    batsman_json = item["batsman"]
    other_batsman_json = item["otherBatsman"]
    bowler_json = item["bowler"]
    ball = innings_json["balls"]
    # total_runs = innings_json["runs"]          # Actual total score

    runs = innings_json["totalRuns"] # Runs for the ball

    wickets = 1 if innings_json["fallOfWickets"] > 0 else 0

    innings.add_runs(runs)
    innings.add_wickets(wickets)

    date = item.get('date', 'Null')

    batsman_info = utils.process_batsman_json(batsman_json)
    other_batsman_info = utils.process_batsman_json(other_batsman_json)
    bowler_info = utils.process_bowler_json(bowler_json)

    ball_data = BallData({
            'date': date,
            'ball': ball,
            'total_runs': innings.total_runs,
            'total_wickets': innings.total_wickets,
            'runs': runs,
            'wickets': wickets,
            'batsman_athlete_id': batsman_info[0],
            'batsman_runs': batsman_info[1],
            'batsman_balls_faced': batsman_info[2],
            'other_batsman_athlete_id': other_batsman_info[0],
            'other_batsman_runs': other_batsman_info[1],
            'other_batsman_balls_faced': other_batsman_info[2],
            'bowler_athlete_id': bowler_info[0],
            'bowler_runs': bowler_info[1],
            'bowler_wickets': bowler_info[2],
            'bowler_balls': bowler_info[3]
            }
    )

    if ball == 1 and not innings.balls:
        # Add a 0th ball on the first spin.
        zero_ball_data = BallData({
            'date': date,
            'ball': 0,
            'total_runs': 0,
            'total_wickets': 0,
            'runs': 0,
            'wickets': 0,
            'batsman_athlete_id': -1,
            'batsman_runs': 0,
            'batsman_balls_faced': 0,
            'other_batsman_athlete_id': -1,
            'other_batsman_runs': 0,
            'other_batsman_balls_faced': 0,
            'bowler_athlete_id': -1,
            'bowler_runs': 0,
            'bowler_wickets': 0,
            'bowler_balls': 0
            }
        )
        # innings_data.append(zero_ball_data)
        innings.add_ball(zero_ball_data)
    # innings_data.append(ball_data)
    innings.add_ball(ball_data)


def get_ball_by_ball_scores(
        match,
        scorecard_link,
    ):
    """
    For every match get the ball-by-ball data. For every ball do the following:
    - Get the relevant data points
    - Create Innings
    - Create BallData
    - Store the BallData in a collection
    """

    api_link = utils.get_api_link_from_scorecard_link(scorecard_link)
    for innings_number in (1, 2):
        innings = Innings(
            innings_number=innings_number,
        )
        ball_ids = set()
        #
        page = 1
        page_count = 2
        while page <= page_count:
            link = api_link.format(
                period=innings_number,
                page=page
            )
            logger.info('Loading scorecard page %s from %s', page,
                link)
            # This is cacheable,as we know the match completed
            try:
                data = get_page(link, cacheable=True, as_json=True)
            except ValueError as e:
                logger.warning('Could not get the page from server')
                break

            if 'code' in data:
                logger.warning('Found code %s in data from %s',
                    data['code'], link
                )
                break
            elif 'commentary' not in data or \
                'items' not in data['commentary'] or \
                not data['commentary']['items']:
                logger.warning('No play by play found.')
                break
            else:
                # Set our page info.
                page_count = data['commentary']['pageCount']
                page += 1
                # And pull out the commentary items.
                items = data['commentary']['items']
                for item in items:
                    ball_id = int(item["id"])
                    if ball_id not in ball_ids:
                        add_ball_data(
                            innings,
                            item
                        )
                        ball_ids.add(ball_id)
        if innings.balls:
            match.add_innings(innings)

        # TODO - wat?  Less than max innings perhaps?
        # else:
        #     log_data.append("Tie detected so excluding this from the chasing files")
    return match


def save_match_if_accurate(match):
    if match.innings:
        # print(innings_data)
        accurate = is_accurate_scorecard(
            match
        )
        if accurate:
            # Only save if we have innings, and are accurate.
            match.save()
        else:
            logger.error('Inaccurate match found: %s!', match.match_id)
            # all_matches.append(innings_data)
    else:
        # print(to_append)
        logger.warning('No ball by ball commentary')


def capture_scorecard_data(args, scorecard_link):
    """Processes every scorecard link one by one. The function does the following:
    - Retrieves the json from the input link
    - Figures out whether the match was fully compelted match
    - If it was a fully completed match, gets the first innings score, second innings score, team names, ground name, tournament name
    - Calls get_ball_by_ball_scores to retrieve the ball-by-ball data
    """

    match_id = re.search(
        '/ci/engine/match/(\d+)\.html',
        scorecard_link
    ).group(1)
    if args.norescrape:
        # no_rescrape flag is set. Only scrape the match if I havent already
        # got it
        try:
            match = Match(match_id=match_id, load=True, error_if_exists=True)
        except ValueError as e:
            logger.info("no_rescrape flag set. \
                        Match: %s exists locally. No need to capture data",
                        match_id)
            return

    match = Match(match_id=match_id, load=False, error_if_exists=False)
    logger.info('Capturing scorecard data from %s', scorecard_link)
    # not cacheable, as may be match in progress.
    try:
        soup = get_soup(scorecard_link)
    except ValueError as e:
        logger.warning('Could not get the page %s from server', scorecard_link)
        return

    match_result, is_full_completed_match = utils.is_full_completed_match(soup)
    if not is_full_completed_match:
        logger.warning('Skipping match: %s', match_result)
        return

    first_innings_score = utils.get_innings_scores(soup, 1)
    second_innings_score = utils.get_innings_scores(soup, 2)
    team_names = utils.get_batting_team_names(soup)
    ground_name = utils.get_ground_name(soup)
    tournament_name = utils.get_tournament_name(soup, team_names)


    result = utils.get_result(
        first_innings_score,
        second_innings_score,
        match_result
    )
    # TODO - should we skip ties?
    match.set_match_info({
            'match_result': match_result,
            'result': result,
            'first_innings_score': first_innings_score,
            'second_innings_score': second_innings_score,
            'team_names': team_names,
            'ground_name': ground_name,
            'tournament_name': tournament_name,
        }
    )
    match = get_ball_by_ball_scores(
        match,
        scorecard_link,
    )
    return match


def scrape_match(args, href_link):
    """ Given a link to a particular match, scrape all the relevant information
    """
    match = capture_scorecard_data(args, '{}{}'.format(STATS_PREFIX, href_link))
    if match:
        try:
            (class_id, event_id) = utils.get_class_event_ids(
                STATS_PREFIX + href_link,
                logger)
        except Exception:
            logger.error("Problem with getting class and event ID " + STATS_PREFIX + href_link)
            return

        try:
            (batting_first_team_players, chasing_team_players) = player_utils.get_batting_performance(JSON_LINK,
                                                                                                    class_id,
                                                                                                    event_id,
                                                                                                    logger,
                                                                                                    SAVE_PLAYERS_AS_DICT)
        except Exception as e:
            logger.exception("Problem with class_id: %s, event_id: %s ",
                             class_id,
                             event_id)
            return
            # continue
        if match.innings and len(match.innings) == 2:
            match.innings[0].set_batsmen(batting_first_team_players)
            match.innings[1].set_batsmen(chasing_team_players)
            save_match_if_accurate(match)


def scrape_matches_from_season_link(args, season_link):
    """From the match results page, get every single match result one-by-one
    and call appropriate functions to scrape information
    """
    logger.info('Loading matches from results page: %s', season_link)
    # Not cacheable - more matches will happen.
    try:
        soup = get_soup(season_link)
    except ValueError as e:
        logger.warning('Could not get the page %s', season_link)
        return
    tds = soup.find_all("td", {"nowrap": "nowrap"})
    counter = 0
    for td in tds:
        aas = td.find_all("a", {"class": "data-link"})
        for a in aas:
            tag_text = a.text
            if "T20" in tag_text:
                href_link = a['href']
                scrape_match(args, href_link)


def start_scrape(args, restrict_host_ids=None):
        logger.info('Starting scrape')
        id_values = utils.get_all_id_values(BASE_LINK,
                                            MATCH_TYPE_CLASS,
                                            restrict_host_ids)
        # for id_value in id_values:
        #     print(id_value)
        logger.info('Retrieved all id values')
        season_links = utils.get_season_links(STATS_PREFIX,
                                              MATCH_RESULTS_BASE_LINK,
                                              MATCH_TYPE_CLASS,
                                              id_values)
        logger.info('Retrieved season links')
        for counter, season_link in enumerate(season_links):
            scrape_matches_from_season_link(args, season_link)


def parse_arguments():
    """
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--norescrape",
                        help="Dont rescrape matches I have already scraped \
                        and retrieved information for. Only scrape matches \
                        that are new or unscraped matches for which I \
                        encountered errors.",
                        action="store_true")
    args = parser.parse_args()
    if args.norescrape:
        logger.info("No rescrape flag set")
    else:
        logger.info("No rescrape flag not set")
    return args


# these are moot now, afaics.

# input_file_name = "E:\Python Scripts\cricinfoscraper\\unprocessed_links20180930-115548"

def main(base_input_file_name=None, restrict_host_ids=None):
    args = parse_arguments()
    # if base_input_file_name is None:
    start_scrape(args,
                 restrict_host_ids=restrict_host_ids)
    # else:
        # print("Using input file")
        # start_scrape_with_input_file(base_input_file_name)
    # output_utils.write_all_data(True, input_file_name, all_matches, log_data,
    #                             firstinnings_data_file_name, chasing_data_file_name)


# print(__name__)
# print(logger)
# print(logging.getLoggerClass().root.handlers[0].baseFileName)
if __name__ == '__main__':
    main(base_input_file_name=None, restrict_host_ids=None)
    # main(base_input_file_name=None, restrict_host_ids=("5",))
