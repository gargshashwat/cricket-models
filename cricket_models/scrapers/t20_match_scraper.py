"""
Scrape T20 jsons
Search by year for t20 matches... get class and event ids
put into api to pull and save jsons
These jsons will then be used to find the batting position of a player in the model
"""
from urllib.request import urlopen
from bs4 import BeautifulSoup
from time import sleep
import itertools
import requests
import json
import pandas as pd


def get_id_frame(year,time_sleep=5):
    '''take in a year and output an id dictionary for event ids and class ids
    time_sleep parameter is to insure that we dont get ip adress banned
    used in tandum with get_json'''
    url='http://www.espncricinfo.com/ci/engine/series/index.html?season='+str(year)+';view=season'
    page = urlopen(url)
    soup = BeautifulSoup(page, 'html.parser')
    classes=soup.findAll("div", {"class": "match-section-head"})
    t20_list=[class_div for class_div in classes if 'Twenty20'==class_div.text]
    t20_sections=t20_list[0].nextSibling.next
    links=t20_sections.find_all('a')
    match_links=[]
    for link in links:
        if 'http' not in link.get('href').split(':'): #not a website link
            continue
        page=urlopen(link.get('href'))
        soup = BeautifulSoup(page, 'html.parser')
        series_link=soup.find_all('a')
        match_links.append([link.get('href') for link in series_link if 'SCORECARD' in link.text])
        sleep(time_sleep)
    match_links=list(itertools.chain.from_iterable(match_links))
    class_id=[match.split('/')[2] for match in match_links]
    event_id=[match.split('/')[4] for match in match_links]
    id_frame=pd.DataFrame([event_id,class_id],columns=range(len(class_id)),index=['event_id','class_id']).T
    return id_frame


def json_dump_single(class_id,event_id):
    '''input class id and save json file to current working director'''
    json_url='http://site.web.api.espn.com/apis/site/v2/sports/cricket/'+class_id+'/summary?contentorigin=espn&event='+event_id+'&lang=en&region=uk&section=cricinfo'
    data=requests.get(json_url+'.json')
    with open(class_id+event_id+'.json', 'w') as f:
        json.dump(data.json(), f)


def jason_dump_dictionary(id_frame):
    '''input id_frame which contains class and event ids
    loop through these matches and save them with json_dump_single function'''
    for i,row in id_frame.iterrows():
        json_dump_single(row['class_id'],row['event_id'])


def main(year=2017):
    id_dict=get_id_frame(year)
    jason_dump_dictionary(id_dict)


if __name__ =='__main__':
    main()

