"""
Some (generally static?) settings for use across modules.
"""
import os
import logging

logger = logging.getLogger(__name__)
VAR_DIR = os.path.abspath(
    os.path.join(
        os.path.dirname(
            os.path.realpath(__file__)
        ),
        '..',
        'var',
    )
)
os.makedirs(VAR_DIR, exist_ok=True)
logger.debug('VAR_DIR: %s', VAR_DIR)

CACHE_DIR = os.path.join(VAR_DIR, 'cache')
os.makedirs(CACHE_DIR, exist_ok=True)
logger.debug('CACHE_DIR: %s', CACHE_DIR)

DATA_DIR = os.path.join(VAR_DIR, 'data')
os.makedirs(DATA_DIR, exist_ok=True)
logger.debug('DATA_DIR: %s', DATA_DIR)

PLAYER_DATA_DIR = os.path.join(DATA_DIR, 'players')
os.makedirs(PLAYER_DATA_DIR, exist_ok=True)
logger.debug('PLAYER_DATA_DIR: %s', PLAYER_DATA_DIR)

MATCH_DATA_DIR = os.path.join(DATA_DIR, 'matches')
os.makedirs(MATCH_DATA_DIR, exist_ok=True)
logger.debug('MATCH_DATA_DIR: %s', MATCH_DATA_DIR)

# Whether to ignore the cache (that is, get all fresh url requests)
IGNORE_CACHE = False
