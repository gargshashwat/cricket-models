
import os
import logging
import json

from cricket_models.settings import MATCH_DATA_DIR
from cricket_models.datatypes.innings import Innings

logger = logging.getLogger(__name__)


class Match(object):

    def __init__(self,
                 match_id,
                 load=True,
                 error_if_exists=False,
                 match_info=None):
        self.match_id = match_id

        if load and os.path.exists(self.filename):
            self.load()
            if error_if_exists:
                raise ValueError("Match json exists locally")
        else:
            self.innings = []
            self.match_info = {}
        self.match_info = match_info or self.match_info

        # TODO - init state data, like current innings, batter on strike, etc

    def add_innings(self, innings):
        self.innings.append(innings)

    def set_match_info(self, input_match_info):
        self.match_info = input_match_info
        
    @property
    def filename(self):
        # Shard the file out to avoid having a single directory with a billion
        # files.
        base_dir = os.path.join(*[MATCH_DATA_DIR, ] + list(self.match_id))
        os.makedirs(base_dir, exist_ok=True)
        return os.path.join(base_dir, 'match.json')

    def save(self):
        """
        Write out to JSON
        """
        logger.debug('Saving Match %s', self.match_id)
        # TODO - if there's a JSON decode error, we end up with a blank file
        # Yuck.
        with open(self.filename, 'w') as f:
            json.dump({
                'info': self.match_info,
                'innings': [i.serialize() for i in self.innings],
            }, f)

    def output_to_file(self):
        """
        TODO - this needs to output to a file, as per
        utils/output_utils.py:write_data
        """
        pass

    def load(self):
        """
        Read info in from JSON.
        """
        logger.debug('Loading info on match %s', self.match_id)
        with open(self.filename, 'r') as f:
            data = json.load(f)
            self.match_info = data['info']
            self.innings = []
            for innings in data['innings']:
                self.innings.append(
                    Innings(
                        innings['innings_number'],
                        innings
                    )
                )

    def __str__(self):
        return '<Match {}: {} @ {}>'.format(
            self.match_id,
            ' vs '.join(self.match_info.get('team_names', [])),
            self.match_info.get('ground_name')
        )

    def __repr__(self):
        return str(self)
