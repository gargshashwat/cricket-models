
from cricket_models.datatypes.ball_data import BallData

class Innings(object):

    def __init__(self, innings_number, innings_data=None):
        """
        Load the info about an innings from a dict (from json...)
        """
        self.innings_number = innings_number
        self.total_runs = 0
        self.total_wickets = 0
        if innings_data is not None:
            # Try and load our data from the passed in dict.
            self.balls = []
            for ball in innings_data.get('balls', []):
                self.balls.append(
                    BallData(
                        from_data=ball,
                    )
                )
            self.total_runs = innings_data.get('total_runs', 0)
            self.total_wickets = innings_data.get('total_wickets', 0)
        else:
            self.batsmen = []
            self.balls = []

    def serialize(self):
        return {
            'innings_number': self.innings_number,
            'batsmen': self.batsmen,
            'balls': [b.serialize() for b in self.balls],
            'total_wickets': self.total_wickets,
            'total_runs': self.total_runs,
        }

    def set_batsmen(self, batsmen):
        self.batsmen = batsmen

    def add_ball(self, ball):
        self.balls.append(ball)

    def add_runs(self, runs):
        self.total_runs += runs

    def add_wickets(self, wickets):
        self.total_wickets += wickets

    def __str__(self):
        return '<Innings: {}: {}/{}>'.format(self.innings_number,
             self.total_runs, self.total_wickets)

    def __repr__(self):
        return str(self)
