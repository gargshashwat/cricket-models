
class BallData(object):

    def __init__(self, from_data):
        for field in ('date', 'ball', 'total_runs', 'total_wickets', 'runs',
            'wickets', 'batsman_athlete_id', 'batsman_runs',
            'batsman_balls_faced', 'other_batsman_athlete_id',
            'other_batsman_runs', 'other_batsman_balls_faced',
            'bowler_athlete_id', 'bowler_runs', 'bowler_wickets',
            'bowler_balls'):
            setattr(self, field, from_data.get(field, None))

    def serialize(self):
        return {
            'date': self.date,
            'ball': self.ball,
            'total_runs': self.total_runs,
            'total_wickets': self.total_wickets,
            'runs': self.runs,
            'wickets': self.wickets,
            'batsman_athlete_id': self.batsman_athlete_id,
            'batsman_runs': self.batsman_runs,
            'batsman_balls_faced': self.batsman_balls_faced,
            'other_batsman_athlete_id': self.other_batsman_athlete_id,
            'other_batsman_runs': self.other_batsman_runs,
            'other_batsman_balls_faced': self.other_batsman_balls_faced,
            'bowler_athlete_id': self.bowler_athlete_id,
            'bowler_runs': self.bowler_runs,
            'bowler_wickets': self.bowler_wickets,
            'bowler_balls': self.bowler_balls
        }

    def __str__(self):
        return '<Ball: {}; {} {}R {}W; {}/{}>'.format(
            self.ball,
            self.date,
            self.runs,
            self.wickets,
            self.total_runs,
            self.total_wickets
        )

    def __repr__(self):
        return str(self)
