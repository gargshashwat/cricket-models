import logging
import os
import json

from cricket_models.settings import PLAYER_DATA_DIR

logger = logging.getLogger(__name__)


class Player(object):
    """
    A representation of a player; this can be pushed/pulled out to/from our
    <datastore>.  This is (currently?) a dump out of JSON to the local filesystem.
    """

    def __init__(self, player_id, player_info=None):
        self.player_id = player_id
        if os.path.exists(self.filename):
            self.load()
        else:
            self.innings = {}
            self.player_info = {}
        # Default to the one passed in... perhaps we should save if it's
        # been changed?
        self.player_info = player_info or self.player_info

    @property
    def filename(self):
        # Shard the file out to avoid having a single directory with a billion
        # files.
        base_dir = os.path.join(*[PLAYER_DATA_DIR, ] + list(self.player_id))
        os.makedirs(base_dir, exist_ok=True)
        return os.path.join(base_dir, 'player.json')

    def calculate_summary(self, end_date=None):
        """
        Calculates the summary for all innings up until end date.
        If no end date is given,
        """
        summary = {
            'total_runs': 0,
            'total_outs': 0,
            'total_balls': 0,
            'total_innings': 0,
        }
        # TODO - sort by date, and honour it.
        for event_id, inning in self.innings.items():
            summary['total_balls'] += inning['ballsFaced']
            summary['total_runs'] += inning['runs']
            summary['total_outs'] += inning['outs']
            summary['total_innings'] += 1
        summary['average'] = float(summary['total_runs']) / \
            float(summary['total_outs'] or 1)
        summary['true_average'] = float(summary['total_runs']) / \
            float(summary['total_innings'] or 1)
        if summary['total_balls']:
            summary['strike_rate'] = float(summary['total_runs']) / \
                float(summary['total_balls'])
        else:
            summary['strike_rate'] = 0
        return summary

    def add_innings(self, event_id, innings, save=True):
        """
        Add an innings to this players history - the innings should be a
        json serializable dict
        """
        # self.innings.append(innings)
        self.innings[event_id] = innings
        if save:
            self.save()

    def save(self):
        """
        Write out to JSON
        """
        logger.debug('Saving Player %s', self.player_id)
        with open(self.filename, 'w') as f:
            json.dump({
                'info': self.player_info,
                'innings': self.innings,
            }, f)

    def load(self):
        """
        Read info in from JSON.
        """
        logger.debug('Loading info on player %s', self.player_id)
        with open(self.filename, 'r') as f:
            data = json.load(f)
            self.player_info = data['info']
            self.innings = data['innings']

    def __str__(self):
        return '<Player {}: {}>'.format(
            self.player_info,
            self.player_info.get('name', 'NO NAME'),
        )

    def __repr__(self):
        return str(self)
